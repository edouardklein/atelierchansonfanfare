\language "français"
title = #(define-scheme-function
     (instrument)
     (string?)
     #{
     \header {
      title = "La tactique du gendarme"
      subtitle = #instrument
      }
      #}
      )
fragment = #(define-scheme-function
     (midi-instrument from to music)
     (string? (ly:pitch? #{do#}) (ly:pitch? #{do#}) ly:music?)
     #{
              \score{
              \new Staff {
                     \set Staff.midiInstrument = #midi-instrument
                     \transpose #from #to { #music }
              }
            \midi{}
            \layout{}
        }
      #})

IntroMib = \relative{
  \tempo 4 = 90
  \key do \major
  \repeat volta 2 {
  \partial 4 la' 8 do mi16 mi mi mi mi8 do la4
  la 8 do mi16 mi mi mi mi8 do la4
  la 8 do fa16 fa fa fa fa8 do la4
  la 8 do fa16 fa fa fa fa8 do la4
  }
}

BMib = \relative{
\tempo 4 = 90
\key do \major
r8 si'16 si r si do si la8 mi8~8 r
r8 si'16 si r si do si la8 mi'8~8 r
r8 la,16 la r la si la sol8 mi'8~8 r
r8 la,16 la r la si la la4 r
r8^"La j'arrivais plus à me relire?" la16 la r8 la8 la16 la la8 la4
sol8 la r4. fa16 mi fa4
\bar "|."
}
