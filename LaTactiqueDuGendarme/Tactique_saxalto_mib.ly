\version "2.22.2"
\include "Tactique.ily"
$(title "Saxophone Alto - Mi b")

\markup{\bold Intro}
$(fragment "alto sax" IntroMib)

\markup{\bold B}
$(fragment "alto sax" BMib)

\score {
  {
    \repeat unfold 8 { s1 \break }
  }
}
