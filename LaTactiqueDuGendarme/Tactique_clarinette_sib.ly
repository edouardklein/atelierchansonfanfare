\version "2.22.2"
\include "Tactique.ily"
$(title "Clarinette - Si b")

\markup{\bold Intro}
$(fragment "clarinet" #{sol#} #{do'#} IntroMib)

\markup{\bold B}
$(fragment "clarinet" #{sol#} #{do'#} BMib)

\score {
  {
    \repeat unfold 8 { s1 \break }
  }
}
