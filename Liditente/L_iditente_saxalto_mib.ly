\version "2.22.2"
\include "L_iditente.ily"
$(title "Saxophone Alto - Mi b")

\markup{\bold Intro}
$(fragment "alto sax" IntroMib)

\markup{Les clans des rues [...] quand il se croit français}

\markup{\bold Pont}
$(fragment "alto sax" PontMib)

\markup{Avec ses sans papiers [...] y'a plus rien}

\markup{{\bold Intro} mais avec départ en contretemps pour ne pas masquer le {\italic rien} des chanteurs:}
$(fragment "alto sax" IntroContreTempsMib)

\markup{De rétention en cale de fond [...] quand il se croit français}

\markup{\bold Pont}

\markup{Avec tous ces champs d'tir [...] pars au front}

\markup{{\bold Poooooont} // Paris sera beau [...] Paris sera beau}
$(fragment "alto sax" PoooontMib)

\markup{\bold Envoi}
$(fragment "alto sax" EnvoiMib)
