\version "2.22.2"
\include "L_iditente.ily"
\paper { page-count = #1 }
$(title "Trombone - Ut")

\markup{\bold Intro}

\markup{Les clans des rues [...] quand il se croit français}

\markup{\bold Pont}

\markup{Avec ses sans papiers [...] y'a plus rien}

\markup{{\bold Intro} mais avec départ en contretemps pour ne pas masquer le {\italic rien} des chanteurs:}

\markup{De rétention en cale de fond [...] quand il se croit français}

\markup{\bold Pont}

\markup{{\bold PouetPouetPouet} // Avec tous ces champs d'tir [...] pars au front}

\markup{{\bold Poooooont} // Paris sera beau [...] Paris sera beau}

\markup{\bold Envoi}
$(fragment "trombone" EnvoiUt)
