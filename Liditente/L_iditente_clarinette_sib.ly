\version "2.22.2"
\include "L_iditente.ily"
\paper { page-count = #1 }
$(title "Clarinette - Si b")

\markup{\bold Intro}
% A do on Sax is a Mib on piano. A Sib on Piano is a sol on sax, and a do on clarinet
$(fragment "clarinet" #{sol#} #{do'#} IntroMib)

\markup{Les clans des rues [...] quand il se croit français}

\markup{\bold Pont}
$(fragment "clarinet" PontSib)

\markup{Avec ses sans papiers [...] y'a plus rien}

\markup{{\bold Intro} mais avec départ en contretemps pour ne pas masquer le {\italic rien} des chanteurs:}
$(fragment "clarinet" #{sol#} #{do'#} IntroContreTempsMib)

\markup{De rétention en cale de fond [...] quand il se croit français}

\markup{\bold Pont}

\markup{{\bold PouetPouetPouet} // Avec tous ces champs d'tir [...] pars au front}
$(fragment "clarinet" PouetPouetSib)

\markup{{\bold Poooooont} // Paris sera beau [...] Paris sera beau}
$(fragment "clarinet" #{sol#} #{do'#} PoooontMib)

\markup{\bold Envoi}
$(fragment "clarinet" EnvoiSib)
