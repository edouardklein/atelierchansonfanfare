\language "français"
title = #(define-scheme-function
     (instrument)
     (string?)
     #{
     \header {
      title = "L'iditenté"
      composer = "Les têtes Raides-Noir Désir"
      subtitle = #instrument
      }
      #}
      )
fragment = #(define-scheme-function
     (midi-instrument from to music)
     (string? (ly:pitch? #{do#}) (ly:pitch? #{do#}) ly:music?)
     #{
              \score{
              \new Staff {
                     \set Staff.midiInstrument = #midi-instrument
                     \transpose #from #to { #music }
              }
            \midi{}
            \layout{}
        }
      #})

IntroMib = \relative{
  \tempo 4 = 140
  \key sib \major
  sol'2 sib4 sol fa2 re4 r
  sol4. sol8 sib sol sib sol la2 fad4 r
  sib4. re8 sib re sib re la2 fad4 r
  r8 sol r8 sol sib4 sol fa2 re4 r
  \bar "|."
}

IntroContreTempsMib = \relative{
  \tempo 4 = 140
  \key sib \major
  r8 sol'8~4 sib4 sol ^\markup{le reste iditenque}fa2 re4 r
}

PontMib = \relative{
  \tempo 4 = 140
  \key sib \major
  \repeat volta 2 {
    \partial 2. r8 sib''8 sol sib4. sol4. sib16 sib sib8 sol~ 8 sib8 la4
    r8 la8 fad la4. fad4. la16 la la8 fad~ 8 la8 sib4
  }
}
PontSib = \relative{
\tempo 4 = 140
\key mib \major
\repeat volta 2 {
\partial 2. r8 sol''8 mib sol4. mib4. sol16 sol sol8 mib~ 8 sib' lab4
r8 fa re fa4. re4. fa16 fa fa8 re~ 8  lab'}
\alternative{
    {sol4}
    {sol4 r2.}
    }
r8 la sold la r2
r8 sol fad sol r2
  \bar "|."
}
PouetPouetSib = \relative{
\tempo 4 = 140
\key mib \major
\partial 8 do''16 re mib4 r2 r8
do16 re mib4 r2 r8
si16 do re4 r2 r8
si16 do re4 r2 r8
do16 re mib4 r2 r8
do16 re mib4 r2 r8
fa16 sol lab4 r2 r8
fa16 sol lab4 r2 r8
si16 do re4 r2 r8
si16 do re4 r2 r8
  \bar "|."
}
PoooontMib = \relative{
  \tempo 4 = 100
  \key sib \major
  \repeat volta 2 {
    \partial 2. r8 re''8 sib re4. sib4. re16 re re8 sib~ 8 fa'8 mib4
    r8 do8 la do4. la4. do16 do do8 la~ 8 mib'8}
  \alternative{
    {re4}
    {re1}
    }
  <<fa2. fa,2.>> <<re'8 re,8>> <<fa fa'>> <<{sol2.~8} {sol,2.~8}>> <<re'' re,>> <<do'1 do,1>>
  <<{re'2~ 8} {re,2~ 8}>> <<fa fa'>> <<do, do'>> <<la, la'>>
  <<fa2. fa,2.>> <<re'8 re,8>> <<fa fa'>> <<{sol2.~8} {sol,2.~8}>> <<re'' re,>> <<do'1 do,1>>
  \bar "|."
}
EnvoiSib = \relative{
  \tempo 4 = 140
  \key mib \major
  \repeat volta 2 {
    \partial 8 sol''8 mib sol4. mi4 sol16 sol sol8 mib4 sol8 r8 r2 r1 r1
    } \break
    do,16^"...puis:" si do mib~16 do do si
    do si do mib~16 do do si
    do si do mib~16 do do si
    do si do re~16  do do si
    do si do re~16  do do si
    do si do re~16  do do si
    do si do re~16  do do si
    do si do mib~16 do do si
  \bar "|."
}
EnvoiMib = \relative{
  \tempo 4 = 140
  \key sib \major
  \repeat volta 2 {
    \tuplet 3/2 {re''4 mib fa sol fa mib re mib re} do2
    \tuplet 3/2 {do4 sib la do sib la}}
  \alternative{
      {\tuplet 3/2 {do re mib} re2}
      {\tuplet 3/2 {do4 sib la} sol4 r4}}
  \bar "|."
  }

EnvoiUt = \relative{
  \tempo 4 = 140
  \key reb \major
  \clef F
  \transpose re fa,, \EnvoiMib
}
