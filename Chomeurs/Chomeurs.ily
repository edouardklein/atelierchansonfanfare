\language "français"
title = #(define-scheme-function
     (instrument)
     (string?)
     #{
     \header {
      title = "Chomeurs, Précaires, intemittents, intérimaires"
      subtitle = #instrument
      }
      #}
      )
fragment = #(define-scheme-function
     (midi-instrument from to music)
     (string? (ly:pitch? #{do#}) (ly:pitch? #{do#}) ly:music?)
     #{
              \score{
              \new Staff {
                     \set Staff.midiInstrument = #midi-instrument
                     \transpose #from #to { #music }
              }
            \midi{}
            \layout{}
        }
      #})

UnMib = \relative{
  \tempo 4 = 90
  \key sib \major
  \repeat volta 2 {
  la'4.^"1A" la la4 sib1
  }
  \repeat volta 2 {
  sol4.^"1B" sol sol4 fa1
  }
}

DeuxMib = \relative{
  \tempo 4 = 90
  \key sib \major
  \repeat volta 2 {
  \partial 4
  fa'8.^"2A" mi16 re4. re fa4
  }
  \alternative{{fa2.}{fa2. re4}}
  \repeat volta 2 {
  mib4.^"2B" mib mib4
  }
  \alternative{{si2. re4}{fa2. fa8. mi16}}
}

TroisMib = \relative{
  \tempo 4 = 90
  \key sib \major
  \repeat volta 2 {
  la'8^"3A" fa~8 re~8 re la' fa~1
  }
  \repeat volta 2 {
  sol8^"3B" mib~8 do~8 do sol' sold~1
  }
}
