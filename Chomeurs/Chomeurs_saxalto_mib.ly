\version "2.22.2"
\include "Chomeurs.ily"
$(title "Saxophone Alto - Mi b")

$(fragment "alto sax" UnMib)

$(fragment "alto sax" DeuxMib)

$(fragment "alto sax" TroisMib)

\score {
  {
    \repeat unfold  6 { s1 \break }
  }
}
